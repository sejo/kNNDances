
var display = "";



// Name of the sequence files to usse
var seqName = "test5"; // test5 or test3
var seqNPoses = 900;

// Array for the frames
var frames = [];
var framePtr;
// JSON of poses
var poses;
var files;
var posesNames = [];

// Stores the distances between poses
var distances = [];

// Graph of Nearest Neighbors
var graph = [];

// Original sequence in order
var seqo = [];
var seq2 = [];

var currentPose;

var dnao;

var K = 26;
var P = 2.8; // Power to raise the costs in Dijkstra

var slider;

function preload(){
	var n;
	poses = [];
	// Load poses JSON
	var x;
	/*
	files = [];
	for(var i=1;i<=5;i++){
		seqName = "test"+i;
		x = loadJSON("data/"+seqName+".json");
		files.push(x);
	}
	*/


	poses = loadJSON("data/poses.json");


	// Load frames
	for(var i=0; i<seqNPoses; i++){
		n = ('0000'+i).slice(-4);
		frames[i] = loadImage("data/frames/resized/frame_"+n+".png");
		seqo[i] = i;
//		seq2[i] = floor(random(seqNPoses));
	}



}

function setup() {
	frameRate(30);



	// Sort and load files
	/*
	var p;
	var counter = 0;
	for(var i=0;i<5;i++){
		p = files[i];	
		for(var po in p){
			poses[counter] = p[po];
			counter++;
		}
	}
	
	saveJSON(poses,"poses.json");
	*/

	
	console.log(poses.length);
//	console.log(poses);
	seqNPoses = poses.length;
  
  framePtr = 0;
  console.log("Starting at "+hour()+":"+minute());

  console.log("Calculating distances between poses...");
  calculateDistances();
  console.log("Distances calculated!");

  console.log("Calculating graph of nearest neighbors...");
  calculateGraph(K);
//  console.log(graph);
  console.log("Graph calculated!");

  framePtr = 0;
  display = createP("");
  display.class("results");
  display.position(10,530);

  var canvas = createCanvas(600, 350);
  canvas.parent("canvashere");




	slider = createSlider(1,3,2,0.1);
	slider.parent("sliderhere");
	slider.input(inputSlider);
	P = slider.value();

	var des = select('#sliderdescription');
	des.html("Adjust Fluidity ("+P+")");


	console.log("Calculating shortest path");



	currentPose = 0;
	seq2 = getShortestPath(currentPose, floor(random(seqNPoses)));
	console.log(seq2.length);
//	generateSequence();



}

function inputSlider(){
	P = slider.value();
	var des = select('#sliderdescription');
	des.html("Adjust Fluidity ("+P+")");
}


function draw() {
	background(255);

	drawFrameAndPose(currentPose,0,0);

	if(framePtr<seq2.length-1){
		framePtr++;
		currentPose = seq2[framePtr]; 
	}
	else{
		generateSequence();
	}

//	drawFrameAndPose(seqo[framePtr],0,0);
	// Different ways of getting the next node
//	framePtr = getRandomNeighborIndex(framePtr);
//	framePtr = getLastNeighborIndex(framePtr);
//	framePtr = getProbableNeighborIndex(framePtr);
//	framePtr = getProbableInvertedNeighborIndex(framePtr);
//	console.log(framePtr);

  
}

function generateSequence(){
	var newPose = floor(random(seqNPoses));
	seq2 = getShortestPath(currentPose, newPose);

	framePtr = 0;


}

// Dijkstra algorithm from
// https://github.com/shiffman/NOC-S17-2-Intelligence-Learning/blob/master/week1-graphs/03_dijkstra/sketch.js
//
var costs,parents,processed;
function getShortestPath(a,b){
	console.log("Getting shortest path from "+a+" to "+b);
	// Costs between a and any other node
	costs = {};
	// Array of parents
	parents = {};

	processed = {};

	var node,nodeN;
	// Initialize costs
	for(var i=0;i<seqNPoses; i++){
		costs[i] = Infinity;
//		costs[i] = 1000;
	}
	// Update costs of neighbors 
	for(n in graph[a]){
		node = graph[a][n];
		costs[node.key] = pow(node.value,P);
		parents[node.key] = a+"";
	}

//	console.log(parents);

	// Setup
	var start = a+"";
	var end = b+"";

	node = lowestCost(costs);
	// As long as I still haves somewhere to go
	while (node != undefined) {

	  // What's the cost?
	  var cost = costs[node];
	  // What are the neighbors?
	  var neighbors = graph[node];
	  // Update costs for all neighbors
	  for (n in neighbors) {
		  nodeN = neighbors[n];
	    var newcost = cost + pow(nodeN.value,P);
	    // Is the new cost less?
	    if (costs[nodeN.key] > newcost) {
	      costs[nodeN.key] = newcost;
	      parents[nodeN.key] = node;
	    }
	  }
	  // It's processed
	  processed[node] = true;
	  // Keep going
	  node = lowestCost(costs);
  	}

	var allProcessed = true;
	for(var i=0; i<seqNPoses; i++){
		if(!processed[i]){
			allProcessed = false;
		}
	}

//	console.log(allProcessed);
//	console.log(costs);

	// Check if there are unreachable nodes
	// k should be changed accordingly 
	var containsInfinity = false;
	for(i in costs){
		if(!isFinite(costs[i])){
//		if(costs[i]>=1000){
			containsInfinity = true;
		}
	}
	console.log("Contains infinity (unreachable nodes) "+containsInfinity);
//	console.log(parents);
//	console.log(parents[end]);
	//
	
	while(parents[end]==undefined){
		end = floor(random(seqNPoses))+"";
	}

	var path = [end];
	var next = parents[end];


	// Create path
	while (next!=start) {
		path.push(next);
		next = parents[next];
	}
//	path.push(start);
	reverse(path);
	console.log(path);

	return path;
}

// Find the node with the lowest cost that is not processed
function lowestCost(costs) {
  var record = Infinity;
  var lowest;
  for (node in costs) {
    if (costs[node] < record && !processed[node]) {
      record = costs[node];
      lowest = node;
    }
  }
  return lowest;
}


// Get the index of a completely random neighbor
function getRandomNeighborIndex(i){
	var neighbors = graph[i].slice();
	var rand = floor(random(neighbors.length));
	var item = neighbors[rand];
	return item.key;
}

// Get the index of the farthnest neighbor within the kNN
function getLastNeighborIndex(i){
	var neighbors = graph[i].slice();
	var item = neighbors[neighbors.length-1];
	return item.key;
}

// Get a new index based in probability - the closer the more probable
function getProbableNeighborIndex(i){
	var neighbors = graph[i].slice();
	var results = [];
	var sum = 0;
	var value, ivalue;
	var key;
	for(var i=0; i<neighbors.length; i++){
		value = neighbors[i].value;	
		key = neighbors[i].key;
		ivalue = 1.0/value;
		for(var j=0; j<ivalue; j++){
			results.push(key);
		}
	}
	var rand = floor(random(results.length));
	var item = results[rand];
	return item;
}

// Get a new index based in probability - the closer the less probable
function getProbableInvertedNeighborIndex(i){
	var neighbors = graph[i].slice();
	var results = [];
	var sum = 0;
	var value, ivalue;
	var key;
	for(var i=0; i<neighbors.length; i++){
		value = neighbors[i].value;	
		key = neighbors[i].key;
		ivalue = 100.0*value;
		for(var j=0; j<ivalue; j++){
			results.push(key);
		}
	}
	var rand = floor(random(results.length));
	var item = results[rand];
	return item;
}


// Create the graph of the k Nearest Neighbors
function calculateGraph(k){
	for(var i=0; i<seqNPoses; i++){
		graph[i] = getkNN(i,k);
	}

}

// Get the k Nearest Neighbors of index
function getkNN(index, k){
  // Get array of distances
  var distancesFromOne = distances[index].slice();

  // Sort them by the key "value"
  distancesFromOne.sort(function(a,b){return a.value-b.value;});

  // Return the sliced array from 1 to 1+k
  // From 1 because 0 is the same node
  // To 1+k because slice works while i<1+k
  return distancesFromOne.slice(0,1+k);

}

// Draw frame with index i along with its pose
function drawFrameAndPose(i,x,y){
	image(frames[i],x,y,480,270);
	drawPose(i,x,y,80);

	textSize(20);
	noStroke();
	fill(255,0,0);
	text(i,x+20,y+20);
}

// Draw pose skeleton
function drawPose(name,x,y,s){
	var joints = poses[name];

	noFill();
	stroke(255,100);
	rect(x,y,2.5*s,2.5*s);

	push();

	translate(x+s,y+2*s);
	fill(255,0,0);
	noStroke();
	scale(s);
	for(var j in joints){
		ellipse(joints[j].x,-joints[j].y,0.1,0.1);
	}
	pop();
}


// Create table of distances between poses
function calculateDistances(){
	var dist;
	var j1, j2;
	var v1, v2;
	// For each pose
	for(var pose in poses){
//		console.log("Distances from "+pose);
		distances[pose] = [];
		// Check every other pose
		for(var pose2 in poses){
//			dist = distanceBetweenPoses(pose,pose2);
			dist = euclideanDistanceBetweenPoses(pose,pose2);
//			distances[pose][pose2] = dist;
			distances[pose].push({key:pose2,value:dist});
			

		}
	}

//	console.log(distances);

}

// Sum of euclidean distances of joints
function distanceBetweenPoses(pose, pose2){
	dist = 0;
	// And measure distance for each joint
	for(var joint in poses[pose]){
		j1 = poses[pose][joint];
		j2 = poses[pose2][joint];
		v1 = createVector(j1.x,j1.y,j1.z);
		v2 = createVector(j2.x,j2.y,j2.z);


		dist += p5.Vector.dist(v1,v2);
	}

	return dist;
}

// Euclidean distance between two poses
function euclideanDistanceBetweenPoses(pose, pose2){
	var deltas = [];
	// And measure distance for each joint
	for(var joint in poses[pose]){
		j1 = poses[pose][joint];
		j2 = poses[pose2][joint];

		deltas.push(j1.x-j2.x);
		deltas.push(j1.y-j2.y);
		deltas.push(j1.z-j2.z);
	}

	var dist = 0;
	for(var i=0; i<deltas.length;i++){
		dist += sq(deltas[i]);
	}
	dist = sqrt(dist);

	return dist;
}

